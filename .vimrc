call pathogen#infect()
syntax on
filetype plugin indent on
scriptencoding utf-8
set encoding=utf-8

"Saving,backups
set autoread                    " watch for file changes by other programs
set backup                      " produce *~ backup files
set backupext=~                 " add ~ to the end of backup files
set backupdir=~/tmp             " set where to store backups
set dir=~/tmp                   " set where to store swapfiles
set undodir=~/tmp/undo         " set where undo files are stored

"Misc
set noautowrite                 " don't automatically write on :next, etc
let maplocalleader=','          " all my macros start with ,
set wildmenu                    " : menu has tab completion, etc
set scrolloff=5                 " keep at least 5 lines above/below cursor
set sidescrolloff=5             " keep at least 5 columns left/right of cursor
set history=200                 " remember the last 200 commands
set noscrollbind                " disable scrollbind by default

" window spacing
set cmdheight=1                 " make command line two lines high
"set ruler                      " show the line number on bar
set lazyredraw                  " don't redraw when running macros
"set number                     " show line number on each line
"set winheight=999              " maximize split windows
"set winminheight=0             " completely hide other windws

map <LocalLeader>w+ 100<C-w>+  " grow by 100
map <LocalLeader>w- 100<C-w>-  " shrink by 100

"Global editing settings
"If softtabstop is less than tabstop and expandtab is not set,
"vim will use a combination of tabs and spaces to make up the
"desired spacing. If softtabstop equals tabstop and expandtab
"is not set, vim will always use tabs.
set noexpandtab                 " don't convert tabs to spaces
set tabstop=8                   " tabstops of 8
set shiftwidth=8                " indents of 8
set softtabstop=8               " indentation with mixed tabs and spaces
set textwidth=78                " screen in 80 columns wide, wrap at 78

" ,p and shift-insert will paste the X buffer, even on the command line
nmap <LocalLeader>p i<S-MiddleMouse><ESC>
imap <S-Insert> <S-MiddleMouse>
cmap <S-Insert> <S-MiddleMouse>

"Makes the mouse paste a block of text without formatting it
"(good for code)
map <MouseMiddle> <esc>"*p

"Global editing settings
set autoindent smartindent      " turn on auto/smart indenting
set smarttab                    " make <tab> and <backspace> smarter
set backspace=eol,start,indent  " allow backspaces over indent, eol, & start
set undolevels=1000             " number of forgivable mistakes
set updatecount=100             " write swap file to disk every 100 chars
set complete=.,w,b,u,U,t,i,d    " do lots of scanning on tab completion
set viminfo=%100,'100,/100,h,\"500,:100,n~/.viminfo
nmap <C-J> vip=                 " forces (re)indentation of a block of code.

" searching...
set hlsearch                   " enable search highlight globally
set incsearch                  " show matches as soon as possible
set showmatch                  " show matching brackets when typing
" disable last one highlight
nmap <LocalLeader>nh :nohlsearch<cr>

set diffopt=filler,iwhite       " ignore all whitespace and sync

"Some useful mappings

" disable search complete
let loaded_search_complete = 1

" Y yanks from cursor to $
map Y y$
" toggle list mode
nmap <LocalLeader>tl :set list!<cr>
" toggle paste mode
nmap <LocalLeader>pp :set paste!<cr>
" change directory to that of current file
nmap <LocalLeader>cd :cd%:p:h<cr>
" change local directory to that of current file
nmap <LocalLeader>lcd :lcd%:p:h<cr>

" save and build
nmap <LocalLeader>wm  :w<cr>:make<cr>

"  buffer management, note 'set hidden' above

" Move to next buffer
map <LocalLeader>bn :bn<cr>
" Move to previous buffer
map <LocalLeader>bp :bp<cr>
" List open buffers
map <LocalLeader>bb :ls<cr>

" tabs
map <LocalLeader>tc :tabnew %<cr>    " create a new tab
map <LocalLeader>td :tabclose<cr>    " close a tab
map <LocalLeader>tn :tabnext<cr>     " next tab
map <LocalLeader>tp :tabprev<cr>     " previous tab
map <LocalLeader>tm :tabmove         " move a tab to a new location

" dealing with merge conflicts

" find merge conflict markers
:map <LocalLeader>fc /\v^[<=>]{7}( .*\|$)<CR>


" status line 
set laststatus=2
if has('statusline')
        " Status line detail: (from Rafael Garcia-Suarez)
        " %f		file path
        " %y		file type between braces (if defined)
        " %([%R%M]%)	read-only, modified and modifiable flags between braces
        " %{'!'[&ff=='default_file_format']}
        "			shows a '!' if the file format is not the platform
        "			default
        " %{'$'[!&list]}	shows a '*' if in list mode
        " %{'~'[&pm=='']}	shows a '~' if in patchmode
        " (%{synIDattr(synID(line('.'),col('.'),0),'name')})
        "			only for debug : display the current syntax item name " %=		right-align following items
        " #%n		buffer number
        " %l/%L,%c%V	line number, total number of lines, and column number
        "function! SetStatusLineStyle()
        "        if &stl == '' || &stl =~ 'synID'
        "                let &stl="%f %y%([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]}" .
        "                                        \"%{'~'[&pm=='']}"                     .
        "                                        \"%=#%n %l/%L,%c%V "                   .
        "                                        \"git:%{fugitive#statusline()}"
        "        else
        "                let &stl="%f %y%([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]}" .
        "                                        \" (%{synIDattr(synID(line('.'),col('.'),0),'name')})" .
        "                                        \"%=#%n %l/%L,%c%V "
        "        endif
        "endfunc
        "call SetStatusLineStyle()

        function! SetStatusLineStyle()
                let &stl="%f %y "                       .
                        \"%([%R%M]%)"                   .
                        \"%#StatusLineNC#%{&ff=='unix'?'':&ff.'\ format'}%*" .
                        \"%{'$'[!&list]}"               .
                        \"%{'~'[&pm=='']}"              .
                        \"%="                           .
                        \"%#StatusLineNC#%{fugitive#statusline()}%* " .
                        \"#%n %l/%L,%c%V "              .
                        \""
                 "      \"%#StatusLineNC#%{GetGitBranchInfoString()}%* " .
        endfunc
        call SetStatusLineStyle()

        if has('title')
                set titlestring=%t%(\ [%R%M]%)
        endif

endif

" Solarized
set t_Co=16
syntax enable
"set background=light
set background=dark
colorscheme solarized

"Color
"Special characters such as trailing whitespace, tabs, newlines, when
"displayed using `:set list` can be set to one of three levels depending on 
"your needs. Default value is `normal` with `high` and `low` options.
let g:solarized_visibility="low"
let g:solarized_bold=0

"User functions
"
function! GnuIndent()
    setlocal tabstop=2
    setlocal shiftwidth=2
    setlocal softtabstop=2
    setlocal textwidth=79
    setlocal noexpandtab
    " Set 'formatoptions' to break comment lines but not other lines,
    " and insert the comment leader when hitting <CR> or using "o".
    "setlocal fo-=t fo+=croql
    "gnucc style
    setlocal fo-=ro fo+=cql

    " Set 'comments' to format dashed lists in comments.
    setlocal comments=sO:*\ -,mO:\ \ \ ,exO:*/,s1:/*,mb:\ ,ex:*/

    setlocal cindent
    setlocal cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
    "set cinoptions=>4,n-2,{2,^-2,:0,=2,g0,h2,t0,+2,(0,u0,w1,m1
    "setlocal cinoptions=>2s,e-s,n-s,f0,{s,^-s,:s,=s,g0,+.5s,p2s,t0,(0
endfunction

function! KernelIndent()
    setlocal tabstop=8
    setlocal shiftwidth=8
    setlocal softtabstop=8
    setlocal textwidth=80
    setlocal noexpandtab

    setlocal cindent
    setlocal formatoptions=tcqlron
    setlocal cinoptions=:0,l1,t0,g0
endfunction

nnoremap <silent> <leader>k :SetLinuxFormatting<cr><cr>

"Whitespace, etc
"
set nofoldenable
"highlight ExtraWhitespace ctermbg=red guibg=red
"match ExtraWhitespace /\s\+$/
"autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
"autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
"autocmd InsertLeave * match ExtraWhitespace /\s\+$/
"autocmd BufWinLeave * call clearmatches()

"set list
set cursorline list listchars=tab:»\ ,trail:·,extends:#,nbsp:.
"highlight TabLineFill term=bold cterm=bold
"highlight SpecialKey cterm=none ctermfg=7 gui=none guifg=gray
"highlight NonText ctermfg=7 gui=none guifg=gray



